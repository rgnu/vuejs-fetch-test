const url = 'https://min-api.cryptocompare.com/data/pricemulti'

const doto = function doto(cb) {
  return function(result) {
    cb(result)
    return result
  }
}

const vm = new Vue({
  el: '#app',

  //Mock data for the value of BTC in USD
  data: {
    symbol: null,
    isLoading: false,
    symbols: ['BTC', 'ETH', 'XMR'],
    currencies: ['USD', 'EUR'],
    results: []
  },

  watch: {
    currencies: function() {
      this.fetchData()
    },
    symbols: function() {
      this.fetchData()
    },
    symbol: function(value) {
      this.addSymbol(value)
      this.symbol = null
    }
  },

  computed: {
    url: function() {
      return `${url}?fsyms=${this.symbols.join(',')}&tsyms=${this.currencies.join(',')}`
    }
  },

  methods: {
    fetchData: function() {
      
      Promise
      .resolve(this.url)
      .then(doto(() => this.isLoading = true))
      .then((url) => fetch(url))
      .then(response => response.json())
      .then(data => {
        this.results = data
      })
      .catch(console.error)
      .then(doto(() => this.isLoading = false))
    },

    addSymbol: function(symbol) {
      if (symbol)
        this.symbols.push(symbol.toUpperCase())
    },

    removeSymbol: function(value) {
      if (symbol)
        this.symbols.splice(this.symbols.indexOf(value),1)
    },

    addCurrency: function(currency) {
      if (currency)
        this.currencies.push(currency)
    }
  },

  created: function() {
    this.fetchData()
  }
});
